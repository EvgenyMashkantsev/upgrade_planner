#!/usr/bin/env python3

import sys
import traceback
import yaml
import sqlite3


DEBUG = False
notes = []
errors = []
upgrade_steps = []


def parse_config(filename):
    filename_ = str(filename)
    with open(filename_, 'rt') as file:
        text = file.read()
    result = yaml.safe_load(text)
    for component in result['Versions']:
        if str(component
               ) not in UPGRADE_PLANNER_SUPPORTED_CONFIGURATION_COMPONENTS:
            print("UpgradePlanner supported components:\n" +
                  str(UPGRADE_PLANNER_SUPPORTED_CONFIGURATION_COMPONENTS))
            raise KeyError('Error in ' + filename_ + ': ' + str(component) +
                           " is not component supported by UpgradePlanner")
    return result


def check_configuration_consistency(configuration, database_cursor):
    result = True
    global notes
    for object1 in configuration['Versions'].keys():
        # print(type(version))
        # print(str(version))
        # print(configuration['Versions'])
        # print(configuration['Versions'][object1])
        for object2 in configuration['Versions'].keys():
            if object1 != object2:
                try:
                    is_row_exist = False
                    for row in cur.execute('SELECT * FROM ' + str(object1) +
                                           '_' + str(object2) +
                                           '_comparability_matrix'):
                        # if DEBUG:
                        #    print(str(row))
                        #    print(str(configuration['Versions'][object1]))
                        #    print(str(configuration['Versions'][object2]))
                        if ((str(row[1]) == str(
                            configuration['Versions'][object1])) and
                            (str(row[2]) == str(
                                configuration['Versions'][object2]))):
                            is_row_exist = True
                            if str(row[3]) == 'False':
                                errors.append(str(object1) + ' (version '
                                              + str(row[1]) + ') and ' +
                                              str(object2) + ' (version '
                                              + str(row[2]) +
                                              ') are not comparable')
                                result = False
                            elif str(row[3]) != 'True':
                                notes.append(str(object1) + ' (version '
                                             + str(row[1]) + ') and ' +
                                             str(object2) + ' (version '
                                             + str(row[2]) +
                                             ') are possibly not comparable')
                                result = False
                            elif DEBUG:
                                print(str(object1) + ' (version '
                                      + str(row[1]) +
                                      ') and ' + str(object2) + ' (version '
                                      + str(row[2]) + ') are comparable')
                            if row[4]:
                                notes.append(str(row[4]))
                        if is_row_exist:
                            break
                        # else:
                        #     notes.append('No info about ' + str(object1) +
                        #                  ' (version ' + str(row[1]) +
                        #                  ') and ' + str(object2) +
                        #                  ' (version ' + str(row[2]) +
                        #                  ') comparability')
                except Exception:
                    if str(traceback.format_exc()).find(
                        'no such table') != (-1):
                        continue
                    else:
                        errors.append(traceback.format_exc())
                        result = None
                        # print(traceback.format_exc(), file=sys.stderr)
    return result


is_allow_downgrade = False
if '--allow-downgrade' in sys.argv:
    is_allow_downgrade = True
UPGRADE_PLANNER_SUPPORTED_CONFIGURATION_COMPONENTS = [
    'PostgreSQL', 'Django', 'Python', ]
starter_configuration = parse_config('starter_configuration.yaml')
desired_configuration = parse_config('desired_configuration.yaml')
if DEBUG:
    print(starter_configuration['Versions'])
    print('starter_configuration:')
    print(str(starter_configuration))
    print('desired configuration:')
    print(str(desired_configuration))
    print('desired Django version')
    print(str(desired_configuration['Versions']['Django']))
database = sqlite3.connect('UpgradePlanner.sqlite3')
cur = database.cursor()
try:
    if not check_configuration_consistency(starter_configuration, cur):
        errors.append('Starter configuration is inconsistent')
    if not check_configuration_consistency(desired_configuration, cur):
        errors.append('Desired configuration is inconsistent')
except Exception:
    cur.close()
    exit(1)
if DEBUG:
    try:
        for row in cur.execute('SELECT * FROM ' +
                               'Django_Python_comparability_matrix'):
            print(row)
    except Exception:
        errors.append(traceback.format_exc())
        # print(traceback.format_exc(), file=sys.stderr)
try:
    cur.close()
except Exception:
    pass

# if len(notes) > 0:
#     print('Notes:')
#     for note in notes:
#         print(note)


starter_configuration_output = {"Starter configuration": starter_configuration}
desired_configuration_output = {"Desired configuration": desired_configuration}
upgrade_steps_output = {"Upgrade steps": upgrade_steps}
notes_output = {"Notes": notes}
errors_output = {"Errors": errors}
if len(errors) < 1:
    print(yaml.dump(yaml.safe_load(str(errors_output))))
else:
    print(yaml.dump(yaml.safe_load(str(errors_output))), file=sys.stderr)
print(yaml.dump(yaml.safe_load(str(starter_configuration_output))))
print(yaml.dump(yaml.safe_load(str(desired_configuration_output))))
print(yaml.dump(yaml.safe_load(str(upgrade_steps_output))))
print(yaml.dump(yaml.safe_load(str(notes_output))))
